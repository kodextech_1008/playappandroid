package com.idev.number.playapp_android.networks

import android.annotation.SuppressLint
import android.os.Build
import android.provider.Settings
import com.idev.number.playapp_android.AppMain
import com.idev.number.playapp_android.BuildConfig
import io.ktor.client.HttpClient
import io.ktor.client.call.receive
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.request
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.TextContent
import io.ktor.http.takeFrom
import io.ktor.util.InternalAPI
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

//import com.idev.number.playapp_android.BuildConfig

class BackendGateway(val keychain: KeychainStore) {

    private val client = HttpClient(OkHttp) {
        engine {
            val trustAllCerts =
                arrayOf<X509TrustManager>(@SuppressLint("CustomX509TrustManager") object :
                    X509TrustManager {
                    @SuppressLint("TrustAllX509TrustManager")
                    override fun checkClientTrusted(
                        chain: Array<out java.security.cert.X509Certificate>?, authType: String?
                    ) {
                    }

                    @SuppressLint("TrustAllX509TrustManager")
                    override fun checkServerTrusted(
                        chain: Array<out java.security.cert.X509Certificate>?, authType: String?
                    ) {
                    }

                    override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> =
                        arrayOf()
                })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("TLS")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())

            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)
            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslContext.socketFactory, trustAllCerts[0])
            builder.hostnameVerifier { _, _ -> true } // Allow all hostnames
            builder.addInterceptor(logging)
            this.preconfigured = builder.build()
//            ahs
        }
    }
    private val json = Json.Default
//    fun hasReadPrivilegedPhoneStatePermission(context: Context): Boolean {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            return context.checkSelfPermission(android.Manifest.permission.READ_PRIVILEGED_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
//        } else {
//            return false
//        }
//    }

    private val deviceIdentifier: String?
        @SuppressLint("HardwareIds") get() {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                AppMain.appContext.deviceId.toString()
            } else {
                Settings.Secure.getString(
                    AppMain.appContext.contentResolver, Settings.Secure.ANDROID_ID
                )
            }
        }

//UUID.randomUUID().toString()

    private fun getUrlString(path: String): String {
        return when (BuildInfo.environment) {
            BuildInfo.Environment.Simulator -> "http://localhost:3000/v1$path"
            BuildInfo.Environment.TestFlight -> "https://app-test.phoenix.dubclub.win/v1$path"
            BuildInfo.Environment.DeviceDebug -> "https://app.phoenix.dubclub.win/v1$path"
            else -> "https://app.phoenix.dubclub.win/v1$path"
        }
    }

    suspend fun sendCode(phoneNumber: String): Boolean {
        val payload = LoginRequest(phoneNumber, null)
        val request = request(HttpMethod.Post, "/login/initiate", payload)
        return perform(request)
    }

    suspend fun verifyCode(phoneNumber: String, code: String): LoginResponse {
        val payload = LoginRequest(phoneNumber, code)
        val request = request(HttpMethod.Post, "/login/verify", payload)
        return performNew(request)
    }

    suspend fun handshake(): LoginResponse {
        val request = requestAuthenticated(HttpMethod.Get, "/login/handshake")
        deviceIdentifier?.let {
            request.headers.append("X-Device-ID", it)
        }
        keychain.deviceToken?.let {
            request.headers.append("X-Device-APNS-Token", it)
        }
        return performNew(request)
    }

    suspend fun registerDevice(deviceToken: String): Boolean {
        val payload = RegisterDeviceRequest(deviceToken, deviceIdentifier)
        val request = requestAuthenticated(HttpMethod.Post, "/device/register", payload)
        return perform(request)
    }

    suspend fun deregisterDevice(deviceToken: String): Boolean {
        val payload = RegisterDeviceRequest(deviceToken, deviceIdentifier)
        val request = requestAuthenticated(HttpMethod.Post, "/device/deregister", payload)
        return perform(request)
    }

    suspend fun fetchSamplePlay(): Play {
        val request = requestAuthenticated(HttpMethod.Get, "/plays?sample=1")
        return performNew(request)
    }

    suspend fun fetchPlays(reason: String): PlayListResponse {
        val request = requestAuthenticated(HttpMethod.Get, "/plays?reason=$reason")
        return performNew(request)
    }

    private fun requestAuthenticated(
        method: HttpMethod, path: String, payload: RegisterDeviceRequest
    ): HttpRequestBuilder {
        val request = request(method, path, payload)
        keychain.authToken?.let {
            request.headers.append("Authorization", "Bearer $it")
        }
//        request.headers.append(
//            "Authorization",
//            "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE3MTQ2NjIzMzksInN1YiI6IisxNDM0Mjg0MDMyNSJ9.YL5uwVqLMJnJ1_9B67_3eENhK3ZwtduT7GGdl0ptPSI"
//        )
        return request
    }

    private fun requestAuthenticated(method: HttpMethod, path: String): HttpRequestBuilder {
        val request = request(method, path)
        keychain.authToken?.let {
            request.headers.append("Authorization", "Bearer $it")
        }
//        request.headers.append(
//            "Authorization",
//            "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE3MTQ2NjIzMzksInN1YiI6IisxNDM0Mjg0MDMyNSJ9.YL5uwVqLMJnJ1_9B67_3eENhK3ZwtduT7GGdl0ptPSI"
//        )
        return request
    }


    @OptIn(InternalAPI::class)
    private fun request(
        method: HttpMethod, path: String, payload: LoginRequest
    ): HttpRequestBuilder {
        val data = json.encodeToString(payload)
//        val data = json.encodeToString(Serializable,payload)
        val request = request(method, path)
        request.body = TextContent(data, contentType = ContentType.Application.Json)
        return request
    }

    private fun request(
        method: HttpMethod, path: String, payload: RegisterDeviceRequest
    ): HttpRequestBuilder {
        val data = json.encodeToString(payload)
//        val data = json.encodeToString(Serializable,payload)
        val request = request(method, path)
        request.body = TextContent(data, contentType = ContentType.Application.Json)
        return request
    }

    private fun request(method: HttpMethod, path: String): HttpRequestBuilder {
        return HttpRequestBuilder().apply {
            url.takeFrom(getUrlString(path))
            this.method = method
        }
    }

    private suspend fun perform(urlRequest: HttpRequestBuilder): Boolean {
        val response = client.request<HttpResponse>(urlRequest)
        if (response.status.value != HttpStatusCode.OK.value) {
            throw GatewayError.StatusCode(response.status.value)
        } else {
            return true
        }
    }

    private suspend inline fun <reified T> performNew(urlRequest: HttpRequestBuilder): T {
        val response = client.request<HttpResponse>(urlRequest)
        if (response.status.value != HttpStatusCode.OK.value) {
            throw GatewayError.StatusCode(response.status.value)
        }
        val responseData = response.receive<String>()
//        val res = Gson().fromJson(responseData,PlayListResponse::class.java)
        return json.decodeFromString(responseData)
    }
}

fun groupPlaysByTitle(plays: List<Play>): List<Play> {
    // Step 1: Sort plays by published date in descending order
    val playsParam = plays.sortedByDescending { it.published }
    return playsParam
//    // Step 2: Create a map to store plays grouped by title
//    val playMap = mutableMapOf<String, Play>()
//
//    // Step 3: Populate the playMap with grouped plays
//    for (playItem in playsParam) {
//        var play = playItem.copy()
//
//        if (play.title in playMap) {
//            play = playItem.copy(isSub = true)
//            // If title exists, append the current play to subPlays
//            val existingPlay = playMap[play.title]!!
//
//            if (existingPlay.subPlays == null) {
//                existingPlay.subPlays = arrayListOf()
//            }
//
//            play.isSub = true
//            existingPlay.subPlays?.add(play)
//            playMap[play.title] = existingPlay
//        } else {
//            // If title does not exist, add play as new entry in the map
//            val newPlay = play.copy(subPlays = arrayListOf(play))
//            playMap[play.title] = newPlay
//        }
//    }
//
//    // Step 4: Convert map values to sorted list of plays
//    var groupedPlays = playMap.values.toList().sortedByDescending { it.published }
//
//    // Step 5: Sort each group's subPlays by published date in descending order
//    groupedPlays = groupedPlays.map { group ->
//        if (group.subPlays != null) {
//            val items = group.subPlays?.sortedByDescending { it.published }
//                ?.toMutableList() ?: arrayListOf()
//            group.subPlays?.clear()
//            group.subPlays?.addAll(items)
//        }
//        group
//    }.sortedByDescending { it.published }

//    return groupedPlays
}

@Serializable
data class LoginRequest(val to: String, val code: String?)

@Serializable
data class LoginResponse(
    val valid: Boolean, val token: String? = "", val userid: String? = "", val msg: String? = ""
)

@Serializable
data class RegisterDeviceRequest(
    val token: String,
    val deviceid: String?,
    val device_type: String = "android"
)

@Serializable
data class Play(
    val id: String? = "",
    val title: String = "",
    val body: String? = "",
    val url: String? = "",
    val published: String? = "",
    var read: Boolean? = false,
    val thumbnail: String? = "",
    var subPlays: ArrayList<Play>? = null,
    var isSub: Boolean? = false
) {


    val isAllRead: Int
        get() {
            return subPlays?.filter {
                it.read == false
            }?.count() ?: 0
        }
}//: Date?{}

@Serializable
data class PlayListResponse(val plays: List<Play>)

sealed class BuildInfo {
    sealed class Environment {
        data object Simulator : Environment()
        data object TestFlight : Environment()
        data object DeviceDebug : Environment()
        data object Production : Environment()
    }

    companion object {
        val environment: Environment
            get() {
//                return Environment.Production // Provide default value or fetch from wherever it's being stored
                if (BuildConfig.IS_PRODUCTION) {
                    return Environment.Production // Provide default value or fetch from wherever it's being stored
                } else {
                    return Environment.TestFlight // Provide default value or fetch from wherever it's being stored
                }
            }

    }
}

sealed class GatewayError : Exception() {
    data class StatusCode(val code: Int) : GatewayError()
}
