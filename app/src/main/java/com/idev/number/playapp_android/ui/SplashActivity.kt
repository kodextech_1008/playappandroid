package com.idev.number.playapp_android.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.google.firebase.messaging.FirebaseMessaging
import com.idev.number.playapp_android.MyFirebaseInstanceIDService
import com.idev.number.playapp_android.databinding.ActivitySplashBinding
import kotlinx.coroutines.launch

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseActivity() {
    private lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            mUserRepository?.setupToken(it)
            Log.d(MyFirebaseInstanceIDService.TAG,"${MyFirebaseInstanceIDService.TAG}, token: $it")
        }
        mHandler?.postDelayed({
            if (mUserRepository?.isLoggedIn == true) {
                scopeIO.launch {
                    mUserRepository?.handshake { isGoingNext ->
                        scopeMain.launch {
                            if (isGoingNext) {
                                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                                intent.flags =
                                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                startActivity(intent)
                                finish()
                            } else {
                                startActivity(
                                    Intent(
                                        this@SplashActivity,
                                        NotificationActivity::class.java
                                    )
                                )
                                finish()
                            }
                        }
                    }
                }
            } else {
                startActivity(Intent(this, NotificationActivity::class.java))
                finish()
            }

        }, 2000)
    }
}