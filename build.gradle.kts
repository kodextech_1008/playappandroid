// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    alias(libs.plugins.androidApplication) apply false
    alias(libs.plugins.jetbrainsKotlinAndroid) apply false
    id("org.jetbrains.kotlin.plugin.serialization") version "2.0.0" apply false
    id("io.sentry.android.gradle") version "4.5.0"
    alias(libs.plugins.googleGmsGoogleServices) apply false
}

sentry {
    org.set("dubclub")
    projectName.set("ios-playsapp")

    // this will upload your source code to Sentry to show it as part of the stack traces
    // disable if you don't want to expose your sources
    includeSourceContext.set(true)
}
