package com.idev.number.playapp_android.ui


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.material.snackbar.Snackbar
import com.idev.number.playapp_android.networks.PlayNavigator
import com.idev.number.playapp_android.networks.UserRepository
import com.idev.number.playapp_android.utils.ScreenshotObserver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


open class BaseActivity : FragmentActivity() {
    val getVersionInfo: String
        get() {
            val packageInfo = packageManager.getPackageInfo(packageName, 0)
            val versionName = packageInfo.versionName
            val versionCode =
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                    packageInfo.longVersionCode.toInt()
                } else {
                    packageInfo.versionCode
                }
            return "${versionCode}(${versionName})"
        }
    protected val scopeIO: CoroutineScope = CoroutineScope(Job() + Dispatchers.IO)
    protected val scopeMain: CoroutineScope = CoroutineScope(Job() + Dispatchers.Main)
    val STORAGE_PERMISSION_CODE = 2034
    var mUserRepository: UserRepository? = null
    var mPlayNavigator: PlayNavigator? = null
    val mHandler: Handler?
        get() {
            return Looper.myLooper()?.let { Handler(it) }
        }

    private lateinit var screenshotObserver: ScreenshotObserver
    val androidId: String
        @SuppressLint("HardwareIds") get() {
            return Settings.Secure.getString(
                this.contentResolver, Settings.Secure.ANDROID_ID
            )
        }
    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            scopeIO.launch {
                mUserRepository?.enablePush()
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                val settingsIntent: Intent =
                    Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
                startActivity(settingsIntent)
            }
        }
    }

    fun askPermission() {
        if (ContextCompat.checkSelfPermission(
                this, android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this, android.Manifest.permission.READ_MEDIA_IMAGES
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_MEDIA_IMAGES
                    ), STORAGE_PERMISSION_CODE
                )
            } else {
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    ), STORAGE_PERMISSION_CODE
                )
            }
        } else {
            // Permission is already granted, continue with your operations
            // You can initialize the screenshot observer or perform any other actions
        }
    }

    private fun handleScreenshot(uri: Uri?) {
        // Handle the screenshot URI
        Log.d("scroonShot", "scroonShot happened $uri")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE
        );
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            screenshotObserver = ScreenshotObserver(
                Handler(mainLooper), this
            ) { uri ->
                // Do something when a screenshot is detected
                CoroutineScope(Dispatchers.Main).launch {
                    // Handle the screenshot
                    handleScreenshot(uri)
                }
            }
        }
        if (this !is SplashActivity) {
            askPermission()
        }
        mUserRepository = UserRepository(this, requestPermissionLauncher)
    }

    override fun onStart() {
        super.onStart()
        // Pass in the callback created in the previous step
        // and the intended callback executor (e.g. Activity's mainExecutor).
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
//            val screenCaptureCallback = Activity.ScreenCaptureCallback {
//                // Add logic to take action in your app.
//                Log.d("scroonShot", "scroonShot happened")
//            }
//            registerScreenCaptureCallback(mainExecutor, screenCaptureCallback)
//        } else {
//            screenshotObserver.start()
//        }

    }

    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            val screenCaptureCallback = Activity.ScreenCaptureCallback {
                // Add logic to take action in your app.
                Log.d("scroonShot", "scroonShot happened")
            }
            registerScreenCaptureCallback(mainExecutor, screenCaptureCallback)
        } else {
            screenshotObserver.start()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            val screenCaptureCallback = Activity.ScreenCaptureCallback {
                // Add logic to take action in your app.
                Log.d("scroonShot", "scroonShot happened")
            }
            try {
                unregisterScreenCaptureCallback(screenCaptureCallback)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            screenshotObserver.stop()
        }
    }

    fun Context.showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }


    fun View.showSnakeBar(msg: String) {
        Snackbar.make(this, msg, Snackbar.LENGTH_LONG).show()
    }
}

class AppLifecycleObserver(private val callback: () -> Unit) : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onEnterForeground() {
        callback.invoke()
    }
}