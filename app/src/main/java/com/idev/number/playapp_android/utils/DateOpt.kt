package com.idev.number.playapp_android.utils

import android.util.Log
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs

fun humanizedDate(date: Date?): String {
    val now = Date()
    return humanizedDate(date ?: now, now)
}

fun humanizedDateStr(date: String?): String {
    if (date != null) {
        Log.d("date is","date is 1 ${date}")
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'",Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
        try {
            if(date == "2024-05-05T22:55:40Z"){
                Log.d("Log","Log")
            }
            val dtObject = dateFormat.parse(date)
            val now = Date()
            Log.d("date is","date is 2 ${dtObject} , ${now}")
            val str = humanizedDate(dtObject ?: now, now)
            Log.d("date is","date is 4 ${str}")
            return str
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("date is","date is 3 ${e.localizedMessage}")
        }
    }
    val now = Date()
    return humanizedDate(now, now)
}

fun humanizedDate(date: Date, now: Date): String {
    val calendar = Calendar.getInstance()
    calendar.time = date

    val nowCalendar = Calendar.getInstance()
    nowCalendar.time = now

    val dayDiff = daysBetween(calendar, nowCalendar)
    val hourDiff = hoursBetween(calendar, nowCalendar)
    val minuteDiff = minutesBetween(calendar, nowCalendar)
    Log.d("date is","date is 6 ${dayDiff} , ${hourDiff} , ${minuteDiff}")
    if ((dayDiff > 0 || !isSameDay(calendar, nowCalendar)) && (dayDiff <= 7)) {
        if (dayDiff <= 7) {
            return formattedDayAndTime(date)
        }
        return formattedDateAndTime(date)
    } else if (hourDiff > 0) {
        return formattedTime(date)
    } else if (minuteDiff > 0) {
        return "${minuteDiff}m"
    } else {
        return "now"
    }
}

fun formattedDayAndTime(date: Date): String {
    val formatter = SimpleDateFormat("EEE h:mm a", Locale.getDefault())
    return formatter.format(date)
}

fun formattedDateAndTime(date: Date): String {
    val formatter = SimpleDateFormat("MMM d h:mm a", Locale.getDefault())
    return formatter.format(date)
}

fun formattedTime(date: Date): String {
    val formatter = SimpleDateFormat("h:mm a", Locale.getDefault())
    return formatter.format(date)
}

fun isSameDay(cal1: Calendar, cal2: Calendar): Boolean {
    return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
            cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
            cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH)
}

fun daysBetween(cal1: Calendar, cal2: Calendar): Int {
    val diffInMillis = cal2.timeInMillis - cal1.timeInMillis
    val days = diffInMillis / (1000 * 60 * 60 * 24)
    val hours = (diffInMillis % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    val minutes = (diffInMillis % (1000 * 60 * 60)) / (1000 * 60)
    Log.d("date is","date is 6.5 ${days} , ${hours} , ${minutes}")
    return abs(days.toInt())
}

fun hoursBetween(cal1: Calendar, cal2: Calendar): Int {
    val hour1 = cal2.get(Calendar.HOUR_OF_DAY)
    val hour2 = cal1.get(Calendar.HOUR_OF_DAY)
    return hour1 - hour2
}

fun minutesBetween(cal1: Calendar, cal2: Calendar): Int {
    val minute1 = cal2.get(Calendar.MINUTE)
    val minute2 = cal1.get(Calendar.MINUTE)
    return minute1 - minute2
}
