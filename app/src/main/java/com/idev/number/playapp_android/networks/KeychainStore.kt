package com.idev.number.playapp_android.networks

//import com.github.simonblanke.lib.Keychain
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk

class KeychainStore {

//    private val keychain = Hawk.init(AppMain.appContext).build();

    private val authTokenKey = "win.dubclub.PlaysApp.authToken"
    private val deviceTokenKey = "win.dubclub.PlaysApp.deviceToken"
    private val numberTokenKey = "win.dubclub.PlaysApp.numberToken"
    private val playsIdKey = "win.dubclub.PlaysApp.plays"
    private val userIdKey = "win.dubclub.PlaysApp.userId"

    // Authentication token for making API calls, received after login
    var authToken: String?
        get() = Hawk.get(authTokenKey)
        set(value) {
            if (value != null) {
                try {
                    Hawk.put(authTokenKey, value)
                } catch (e: Exception) {
                    // Handle error
                }
            } else {
                try {
                    Hawk.delete(authTokenKey)
                } catch (e: Exception) {
                    // Handle error
                }
            }
        }

    var verifiedphoneNumber: String?
        get() = Hawk.get(numberTokenKey)
        set(value) {
            if (value != null) {
                try {
                    Hawk.put(numberTokenKey, value)
                } catch (e: Exception) {
                    // Handle error
                }
            } else {
                try {
                    Hawk.delete(numberTokenKey)
                } catch (e: Exception) {
                    // Handle error
                }
            }
        }

    // APNS device token, encoded as a hexadecimal string
    var deviceToken: String?
        get() = Hawk.get(deviceTokenKey)
        set(value) {
            if (value != null) {
                try {
                    Hawk.put(deviceTokenKey, value)
                } catch (e: Exception) {
                    // Handle error
                }
            } else {
                try {
                    Hawk.delete(deviceTokenKey)
                } catch (e: Exception) {
                    // Handle error
                }
            }
        }

    private val gson = Gson()

    fun savePlays(value: List<Play>, userPhone: String?) {
        try {
            val playsData = gson.toJson(value)
            val key = "${playsIdKey}.${userPhone ?: authToken ?: ""}"
            // Example: Use SharedPreferences for storing data
            try {
                Hawk.put(key, playsData)
            } catch (e: Exception) {
                // Handle error
            }
//            val sharedPreferences =
//                context.getSharedPreferences("plays_storage", Context.MODE_PRIVATE)
//            sharedPreferences.edit().putString(key, playsData).apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getPlays(userPhone: String?): List<Play> {
        val key = "${playsIdKey}.${userPhone ?: authToken ?: ""}"
        // Example: Use SharedPreferences for retrieving data
//        val sharedPreferences = context.getSharedPreferences("plays_storage", Context.MODE_PRIVATE)
        val playsData:String? = Hawk.get(key, null)
        return if (!playsData.isNullOrBlank()) {
            try {
                gson.fromJson(playsData, Array<Play>::class.java).toList()
            } catch (e: Exception) {
                e.printStackTrace()
                emptyList()
            }
        } else {
            emptyList()
        }
    }
}

