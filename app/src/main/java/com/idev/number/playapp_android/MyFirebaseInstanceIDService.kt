package com.idev.number.playapp_android

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.idev.number.playapp_android.networks.BackendGateway
import com.idev.number.playapp_android.networks.KeychainStore

class MyFirebaseInstanceIDService : FirebaseMessagingService() {
    companion object {
        const val TAG = "FIREBASE_MSG"
    }
    private val gateway: BackendGateway = BackendGateway(KeychainStore())
    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG,"${TAG}, token: $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d(TAG, "From: ${remoteMessage.from}")

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: ${remoteMessage.data}")

//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use WorkManager.
////                scheduleJob()
//            } else {
//                // Handle message within 10 seconds
//                handleNow()
//            }
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")
        }
    }
}
