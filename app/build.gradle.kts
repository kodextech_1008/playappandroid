plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.jetbrainsKotlinAndroid)
    id("kotlin-kapt")
    id("io.sentry.android.gradle")
//    kotlin("plugin.serialization") version "1.6.0" // Replace with the version you're using
    id("org.jetbrains.kotlin.plugin.serialization" )
    alias(libs.plugins.googleGmsGoogleServices)
}

android {
    namespace = "com.idev.number.playapp_android"
    compileSdk = 35

    defaultConfig {
        applicationId = "com.dubclub.playsapp"
        minSdk = 24
        targetSdk = 35
        versionCode = 7
        versionName = "1.6"
        setProperty("archivesBaseName", "DubClub-ver$versionCode($versionName)")
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildFeatures {
        viewBinding = true
        dataBinding = true
    }
    buildTypes {
        debug {
            buildConfigField("Boolean", "IS_PRODUCTION", "false")
        }
        release {
            buildConfigField("Boolean", "IS_PRODUCTION", "true")
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        named("debug") {
            buildConfigField("Boolean", "IS_PRODUCTION", "false")
        }
        named("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            buildConfigField("Boolean", "IS_PRODUCTION", "true")
        }
    }


    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    implementation(libs.androidx.activity)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.androidx.swiperefreshlayout)
    implementation(libs.firebase.messaging)
    testImplementation(libs.junit)
    implementation(libs.sdp.android)
    implementation(libs.ccp)
    implementation(libs.toggle)
    implementation(libs.circleimageview)
    implementation(project(":apiwrapper"))
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
    implementation(libs.posthog.android)
    implementation(libs.ktor.client.okhttp) // Ktor with OkHttp client
    implementation(libs.ktor.client.serialization) // Ktor client with kotlinx.serialization support
    implementation(libs.kotlinx.serialization.json) // kotlinx.serialization
    implementation(libs.okhttp) // OkHttp
    implementation(libs.logging.interceptor)
    implementation(libs.hawk)
    implementation(libs.secure.preferences.lib)
    implementation(libs.androidx.preference.ktx)
    implementation(libs.glide)
    implementation(libs.ktloadingbutton)
    implementation(libs.google.firebase.messaging.directboot)

//    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.0")
}