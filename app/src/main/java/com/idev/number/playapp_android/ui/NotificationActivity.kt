package com.idev.number.playapp_android.ui

import android.content.Intent
import android.os.Bundle
import com.idev.number.playapp_android.databinding.ActivityNotificationEnableBinding
import kotlinx.coroutines.launch

class NotificationActivity : BaseActivity() {
    private lateinit var binding: ActivityNotificationEnableBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationEnableBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setListener()

    }

    private fun setListener() {
        binding.cardLetsGo.setOnClickListener {
            scopeIO.launch {
                mUserRepository?.enablePush()
                if (mUserRepository?.pushEnabled == true) {
                    scopeMain.launch {
                        startActivity(
                            Intent(
                                this@NotificationActivity,
                                WelcomeActivity::class.java
                            )
                        )
                        finish()
                    }
                }
            }

        }
    }
}