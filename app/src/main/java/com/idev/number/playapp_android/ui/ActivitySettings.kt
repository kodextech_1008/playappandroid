package com.idev.number.playapp_android.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import com.idev.number.playapp_android.R
import com.idev.number.playapp_android.databinding.ActivitySettingsBinding
import kotlinx.coroutines.launch


class ActivitySettings : BaseActivity() {
    private lateinit var binding: ActivitySettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setLabelData()
        setListener()
        binding.tvPhone.text = PhoneNumberUtils.formatNumber(mUserRepository?.verifiedNumber) // mUserRepository?.verifiedNumber
    }


    private fun setListener() {
        binding.icBack.setOnClickListener {
            finish()
        }
        binding.txtContact.setOnClickListener {
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("http://www.dubclub.zendesk.com"))
            startActivity(browserIntent)
        }
        binding.rlSignout.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setMessage("Do you want to logout?")
                .setCancelable(false)
                .setPositiveButton("Yes") { _, _ ->
                    // Perform the logout operation here
                    scopeIO.launch{
                        mUserRepository?.disablePush()
                    }
                    mUserRepository?.logout()
                    val intent = Intent(this, NotificationActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()
                }
                .setNegativeButton("No") { dialog, _ ->
                    dialog.dismiss()
                }

            val alert = dialogBuilder.create()
            alert.setTitle("Logout")
            alert.show()
        }
    }

    private fun setLabelData() {
        binding.label.labelOff = ""
        binding.label.labelOn = ""
        binding.label.colorOff = ResourcesCompat.getColor(resources, R.color.white, null)
        binding.label.colorOn = ResourcesCompat.getColor(resources, R.color.main_color, null)
        binding.label.colorBorder =
            ResourcesCompat.getColor(resources, android.R.color.transparent, null)
    }
}