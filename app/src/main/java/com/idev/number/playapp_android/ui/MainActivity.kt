package com.idev.number.playapp_android.ui

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ProcessLifecycleOwner
import com.idev.number.playapp_android.databinding.ActivityMainBinding
import com.idev.number.playapp_android.networks.PlayNavigator
import com.idev.number.playapp_android.networks.RefreshReason
import kotlinx.coroutines.launch

class MainActivity : BaseActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        mPlayNavigator = PlayNavigator(binding = binding)
        mPlayNavigator?.setupRecyclerView()
        setContentView(binding.root)
        setListener()
        getListing()
        val appLifecycleObserver = AppLifecycleObserver {
            if (mPlayNavigator?.isEmpty == true) {
                mPlayNavigator?.refreshPlayList(RefreshReason.APP_FOREGROUND)
            }
        }
        ProcessLifecycleOwner.get().lifecycle.addObserver(appLifecycleObserver)
        scopeIO.launch {
            mUserRepository?.registerDevice()
        }
    }

    override fun onResume() {
        super.onResume()
        mPlayNavigator?.simpleRefresh()
    }

    private fun getListing() {
        if (mPlayNavigator?.isEmpty == true) {
            mPlayNavigator?.refreshPlayList(RefreshReason.FIRST_LOAD)
        }else{
            mPlayNavigator?.simpleRefresh()
        }
    }

    private fun setListener() {
        binding.hamburger.setOnClickListener {
            startActivity(Intent(this, ActivitySettings::class.java))
        }
        binding.rlFresh.setOnRefreshListener {
            binding.rlFresh.isRefreshing = false
            mPlayNavigator?.refreshPlayList(RefreshReason.DRAG_REFRESH)
        }
    }

}