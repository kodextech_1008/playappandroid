package com.idev.number.playapp_android.ui

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.idev.number.playapp_android.adapter.MyPlaysAdapter
import com.idev.number.playapp_android.databinding.ActivitySubListingBinding
import com.idev.number.playapp_android.networks.Play

class SubListingActivity : BaseActivity() {
    lateinit var binding: ActivitySubListingBinding
    var mList = ArrayList<Play>()
    companion object{
        var subListItem: Play? = null
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySubListingBinding.inflate(layoutInflater)
//        val subListItem: Play? = intent?.extras?.getSerializable("PLAY") as? Play
        mList.clear()
        subListItem?.subPlays?.let { mList.addAll(it) }
        setContentView(binding.root)
        setListener()
        setupRecyclerView()
        binding.tvTitle.text = subListItem?.title
    }

    var mAdapter: MyPlaysAdapter? = null
    fun setupRecyclerView() {
        binding.playsRecycler.layoutManager =
            LinearLayoutManager(binding.root.context, RecyclerView.VERTICAL, false)
        mAdapter = MyPlaysAdapter(binding.root.context, mList, true)
        binding.playsRecycler.adapter = mAdapter
    }


    private fun setListener() {
        binding.btnBack.setOnClickListener {
            finish()
        }
        binding.rlFresh.setOnRefreshListener {
            binding.rlFresh.isRefreshing = false
        }
    }

}