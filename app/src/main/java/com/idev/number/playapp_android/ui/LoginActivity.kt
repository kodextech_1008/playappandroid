package com.idev.number.playapp_android.ui

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import com.hbb20.CountryCodePicker
import com.idev.number.playapp_android.R
import com.idev.number.playapp_android.databinding.ActivityLoginBinding
import kotlinx.coroutines.launch

@Suppress("SameParameterValue")
class LoginActivity : BaseActivity() {
    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setListener()
        setTTFfont(binding.picker, "popins_medium.ttf")
    }

    private fun setTTFfont(ccp: CountryCodePicker, fontFileName: String) {
        val typeFace = Typeface.createFromAsset(this.assets, fontFileName)
        ccp.setTypeFace(typeFace)
    }

    private fun setListener() {
//        binding.picker.setAutoDetectedCountry(true)
//        binding.picker.set
//        binding.numberText.addTextChangedListener {
//            if (it?.length == 10) {
//                binding.cardNext.alpha = 1f
//            } else {
//                binding.cardNext.alpha = 0.6f
//            }
//        }
//        binding.picker.registerPhoneNumberTextView(edtPhoneNumber);
        binding.picker.registerCarrierNumberEditText(binding.numberText)
        binding.picker.setPhoneNumberValidityChangeListener {
            if (it || binding.numberText.text.toString().trim().isEmpty()) {
                if (it) {
                    binding.cardNext.alpha = 1f
                    binding.cardNext.isEnabled = true
                } else {
                    binding.cardNext.isEnabled = false
                    binding.cardNext.alpha = 0.6f
                }
                binding.llMain.background.setTint(getColor(R.color.main_color))
            } else {
                binding.cardNext.alpha = 0.6f
                binding.llMain.background.setTint(getColor(android.R.color.holo_red_dark))
            }
        }
        binding.btnLoader.setOnClickListener {
            binding.cardNext.performClick()
        }
        binding.cardNext.setOnClickListener {
            binding.cardNext.isEnabled = false
            binding.btnLoader.startLoading()
            scopeIO.launch {
                if (mUserRepository?.sendAuthCode(binding.picker.fullNumberWithPlus) == true) {

                    scopeMain.launch {
                        binding.btnLoader.reset()
                        binding.cardNext.isEnabled = true
                        startActivity(
                            Intent(
                                this@LoginActivity,
                                VerifyCodeActivity::class.java
                            ).putExtra("PHONE", binding.picker.fullNumberWithPlus)
                        )
                    }
                } else {
                    scopeMain.launch {
                        binding.btnLoader.reset()
                        binding.cardNext.isEnabled = true
                    }
                }
            }


        }
        binding.icBack.setOnClickListener { finish() }
    }
}