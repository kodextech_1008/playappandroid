package com.idev.number.playapp_android.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.webkit.CookieManager
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.idev.number.playapp_android.databinding.ActivityPlayDetailBinding

class PlayDetailActivity : BaseActivity() {
    private val cookieManager: CookieManager = CookieManager.getInstance()
    private val url: String
        get() {
            return intent?.extras?.getString("URL", "") ?: ""
        }
    private val id: String
        get() {
            return intent?.extras?.getString("ID", "") ?: ""
        }

    private lateinit var binding: ActivityPlayDetailBinding

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setListener()
        val webView = binding.webView
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false
        webView.settings.setSupportZoom(true)
        webView.settings.apply {
            javaScriptEnabled = true
            loadWithOverviewMode = true
            useWideViewPort = true
            builtInZoomControls = true
            displayZoomControls = false
            setSupportZoom(true)
            mediaPlaybackRequiresUserGesture = false // Allow autoplay of media
            mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
        webView.settings.userAgentString = "DubClub/${getVersionInfo}"
        webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
        webView.settings.domStorageEnabled = true
        webView.settings.loadsImagesAutomatically = true
        webView.settings.mixedContentMode = WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE
        cookieManager.setAcceptThirdPartyCookies(webView, true)
        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(
                view: WebView?,
                newProgress: Int
            ) {
                super.onProgressChanged(view, newProgress)
                Log.d("onProgressChanged", "onProgressChanged $newProgress")
                binding.loader.setProgress(newProgress, true)
                binding.loader.isVisible = newProgress < 85
            }
        }
        binding.loader.isIndeterminate = false
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {

                val localLink = Uri.parse(url)
                val clickedLink = request.url
                if (localLink.lastPathSegment == clickedLink.lastPathSegment) {
                    view.loadUrl(request.url.toString())
                    return false
                } else {
//                    view.goBack()
                    val link = Intent(Intent.ACTION_VIEW, clickedLink)
                    startActivity(link)
                    return true
                }

            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                // Your page finished loading logic here
                binding.loader.isVisible = false
            }
        }
        binding.loader.isVisible = true
        webView.loadUrl(url)
    }

    private fun setListener() {
        binding.icBack.setOnClickListener {
            finish()
        }
    }
}