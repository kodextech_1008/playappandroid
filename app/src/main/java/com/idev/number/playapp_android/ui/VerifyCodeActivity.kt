package com.idev.number.playapp_android.ui

import android.content.Intent
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import com.idev.number.playapp_android.databinding.ActivityVerifyCodeBinding
import kotlinx.coroutines.launch

class VerifyCodeActivity : BaseActivity() {
    private lateinit var binding: ActivityVerifyCodeBinding
    private val phone: String
        get() {
            return intent?.extras?.getString("PHONE", "") ?: ""
        }
    private val code: String
        get() {
            return binding.numberText.text.toString().trim()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVerifyCodeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setListener()

    }

    private fun checkBtn(txt: String) {
        if (txt.length == 6) {
            binding.cardNext.isEnabled = true
            binding.cardNext.alpha = 1.0f
        } else {
            binding.cardNext.isEnabled = false
            binding.cardNext.alpha = 0.6f
        }
    }

    private fun setListener() {
        binding.numberText.addTextChangedListener {
            val txt = it.toString().trim()
            checkBtn(txt)
        }
        checkBtn(code)
        binding.cardNext.setOnClickListener {
            binding.cardNext.isEnabled = false
            binding.loader.isVisible = true
            scopeIO.launch {
                if (mUserRepository?.verify(phone, code) == true) {
                    scopeMain.launch {
                        binding.loader.isVisible = false
                        binding.cardNext.isEnabled = true
                        val intent = Intent(this@VerifyCodeActivity, MainActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                        finish()
                    }
                } else {
                    scopeMain.launch {
                        binding.loader.isVisible = false
                        binding.cardNext.isEnabled = true
                    }
                }
            }
        }
        binding.cardNextReset.setOnClickListener {
            binding.cardNextReset.isEnabled = false
            binding.loader.isVisible = true
            scopeIO.launch {
                mUserRepository?.sendAuthCode(phone)
                scopeMain.launch {
                    binding.loader.isVisible = false
                    showToast("Code Resent")
                    binding.cardNextReset.isEnabled = true
                }
            }
        }
        binding.icBack.setOnClickListener { finish() }
    }
}