package com.libraries.coders.comcodersapiwrapper


/**
 * Created by 2Coders on 08/06/2018.
 */

import android.util.Log
import com.github.kittinunf.fuel.core.FileDataPart
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.core.interceptors.LogRequestAsCurlInterceptor
import com.github.kittinunf.fuel.core.requests.upload
import com.github.kittinunf.fuel.httpDelete
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPatch
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpPut
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import org.json.JSONObject
import java.io.File


typealias ParamItem = Pair<String, Any?>

class API {
    enum class RequestMethod(val value: String) {
        GET("GET"),
        HEAD("HEAD"),
        POST("POST"),
        PUT("PUT"),
        DELETE("DELETE"),
        OPTIONS("OPTIONS"),
        TRACE("TRACE"),
        PATCH("PATCH")
    }

    companion object {
        var timeout = 30000
        private val mapper = Gson()
        var LOG_DEBUG_KEY = "API DEBUG KEY "

        fun initManager(
            basePath: String,
            debug: Boolean = false,
            baseHeaders: Map<String, String>
        ) {
            FuelManager.instance.basePath = basePath
            FuelManager.instance.baseHeaders = baseHeaders
            if (debug) {
                logAPI()
            }
        }

        private fun logAPI() {

            FuelManager.instance.addRequestInterceptor(LogRequestAsCurlInterceptor)
            FuelManager.instance.addResponseInterceptor { next: (Request, Response) -> Response ->
                { req: Request, res: Response ->
                    Log.d(LOG_DEBUG_KEY, "REQUEST COMPLETED: $req")
                    Log.d(LOG_DEBUG_KEY, "RESPONSE: $res")
                    next(req, res)
                }
            }
        }

        fun request(
            endPoint: String,
            onSuccess: (resultResponse: String) -> Unit = { },
            onFailure: (resultResponse: String, retryAction: () -> Unit?, statusCode: Int) -> Unit,
            params: List<Pair<String, Any?>>? = null,
            body: Any? = null,
            headers: Map<String, Any> = HashMap(),
            method: RequestMethod
        ) {

            val request = when (method) {
                RequestMethod.GET -> endPoint.httpGet(params).header(headers).timeout(timeout)
                    .timeoutRead(timeout)

                RequestMethod.POST -> endPoint.httpPost(params).header(headers).timeout(timeout)
                    .timeoutRead(timeout)

                RequestMethod.PUT -> endPoint.httpPut(params).header(headers).timeout(timeout)
                    .timeoutRead(timeout)

                RequestMethod.DELETE -> endPoint.httpDelete(params).header(headers).timeout(timeout)
                    .timeoutRead(timeout)

                RequestMethod.PATCH -> endPoint.httpPatch(params).header(headers).timeout(timeout)
                    .timeoutRead(timeout)

                else -> null
            }
            val bod: String = if (body !is JSONObject) mapper.toJson(body)
            else body.toString()

            request?.body(bod)?.responseString { _, response, result ->
                when (result) {
                    is Result.Failure -> {
                        onFailure(
                            String(response.data),
                            {
                                request(
                                    endPoint,
                                    onSuccess,
                                    onFailure,
                                    params,
                                    body,
                                    headers,
                                    method
                                )
                            },
                            response.statusCode
                        )
                    }

                    is Result.Success -> {
                        manageSuccess(onSuccess, result)
                    }
                }
            }
        }

        fun uploadFile(
            endPoint: String,
            headers: Map<String, Any> = HashMap(),
            method: RequestMethod,
            params: List<Pair<String, Any?>>? = null,
            file: File,
            onSuccess: (resultResponse: String) -> Unit = { },
            onFailure: (resultResponse: String, retryAction: () -> Unit?, statusCode: Int) -> Unit
        ) {
            val request = when (method) {
                RequestMethod.GET -> endPoint.httpGet(params).header(headers).timeout(timeout)
                    .timeoutRead(timeout)

                RequestMethod.POST -> endPoint.httpPost(params).header(headers).timeout(timeout)
                    .timeoutRead(timeout)

                RequestMethod.PUT -> endPoint.httpPut(params).header(headers).timeout(timeout)
                    .timeoutRead(timeout)

                RequestMethod.DELETE -> endPoint.httpDelete(params).header(headers).timeout(timeout)
                    .timeoutRead(timeout)

                RequestMethod.PATCH -> endPoint.httpPatch(params).header(headers).timeout(timeout)
                    .timeoutRead(timeout)

                else -> null
            }

            request?.upload()
                ?.add(FileDataPart(file, name = "file"))
                ?.header(headers)
                ?.responseString { _, response, result ->
                    when (result) {
                        is Result.Failure -> {
                            onFailure(
                                String(response.data),
                                {
                                    uploadFile(
                                        endPoint,
                                        headers,
                                        method,
                                        params,
                                        file,
                                        onSuccess,
                                        onFailure
                                    )
                                },
                                response.statusCode
                            )
                        }

                        is Result.Success -> {
                            manageSuccess(onSuccess, result)
                        }
                    }
                }
        }


        private fun manageSuccess(
            onSuccess: (resultResponse: String) -> Unit,
            result: Result<String, FuelError>
        ) {
            onSuccess(result.component1() ?: "")
        }


    }
}