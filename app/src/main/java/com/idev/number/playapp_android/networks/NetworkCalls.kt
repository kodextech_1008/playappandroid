package com.idev.number.playapp_android.networks

import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.idev.number.playapp_android.AppMain
import com.libraries.coders.comcodersapiwrapper.API
import org.json.JSONObject
import java.io.File

typealias onErrorCase = ((msg: String, statusCode: Int) -> Unit)
typealias onSuccessCase = ((response: String, msg: String) -> Unit)

object NetworkCalls {
    private val token: String
        get() {
            return ""//?AppMain.appPref?.loggedInUser?.access_token ?: ""
        }
    private val basicHeaders: HashMap<String, String>
        get() {
            return hashMapOf(
                Pair("Authorization", "Bearer $token"),
                Pair("Content-Type", "application/json")
            )
        }
    private val basicHeadersFile: HashMap<String, String>
        get() {
            return hashMapOf(
                Pair("Authorization", "Bearer $token")
            )
        }

    fun getProviderAndClinic(
        onSuccessCase: onSuccessCase,
        onErrorCase: onErrorCase
    ) {
        API.request(
            endPoint = ApiEndParams.GET_PROVIDER.value,
            method = API.RequestMethod.GET,
            onSuccess = { response ->
                println("DEBUG RESPONSE $response")
                checkIsSuccess(response) { check, message, data ->
                    if (check) {
                        onSuccessCase(data, message)
                    } else {
                        onErrorCase(message, 400)
                    }
                }
            },
            onFailure = { resultResponse, _, statusCode ->
                println("DEBUG ERROR $resultResponse $statusCode")
                onErrorCase(resultResponse, statusCode)
            }
        )
    }

    fun doLogin(
        email: String,
        password: String,
        onSuccessCase: onSuccessCase,
        onErrorCase: onErrorCase
    ) {
        API.request(
            endPoint = ApiEndParams.LOGIN.value,
            method = API.RequestMethod.POST,
            body = JSONObject().put("email", email).put("password", password),
            onSuccess = { response ->
                println("DEBUG RESPONSE $response")
                checkIsSuccess(response) { check, message, data ->
                    if (check) {
                        onSuccessCase(data, message)
                    } else {
                        onErrorCase(message, 400)
                    }
                }
            },
            onFailure = { resultResponse, _, statusCode ->
                println("DEBUG ERROR $resultResponse $statusCode")
                onErrorCase(resultResponse, statusCode)
            }
        )
    }

    fun setAppointmentData(
        appointmentId: String,
        weight: String,
        height: String,
        oxygen: String,
        ecg: String,
        heartRate: String,
        temperature: String,
        bloodPressure: String,
        sugar: String,
        onSuccessCase: onSuccessCase,
        onErrorCase: onErrorCase
    ) {

        API.request(
            endPoint = "${ApiEndParams.SET_APPOINTMENT_DATA.value}${appointmentId}/examination",
            method = API.RequestMethod.PUT,
            body = JSONObject()
                .put("weight", weight)
                .put("height", height)
                .put("oxygen", oxygen)
                .put("ecg", ecg)
                .put("temperature", temperature)
                .put("blood_pressure", bloodPressure)
                .put("heart_rate", heartRate)
                .put("blood_sugar_level", sugar),
            headers = basicHeaders,
            onSuccess = { response ->
                println("DEBUG RESPONSE $response")
                checkIsSuccess(response) { check, message, data ->
                    if (check) {
                        onSuccessCase(data, message)
                    } else {
                        onErrorCase(message, 400)
                    }
                }
            },
            onFailure = { resultResponse, _, statusCode ->
                println("DEBUG ERROR $resultResponse $statusCode")
                onErrorCase(resultResponse, statusCode)
            }
        )
    }


    fun removeQuotesFromJsonObject(jsonObject: JsonObject): JsonObject {
        val modifiedJsonObject = JsonObject()
        for ((key, element) in jsonObject.entrySet()) {
            if (element.isJsonPrimitive && element.asJsonPrimitive.isString) {
                val value = element.asString.trim('"')
                modifiedJsonObject.add(key, JsonPrimitive(value))
            } else {
                modifiedJsonObject.add(key, element)
            }
        }
        return modifiedJsonObject
    }

    fun getAppointmentInfo(
        facilityId: String,
        onSuccessCase: onSuccessCase,
        onErrorCase: onErrorCase
    ) {
        API.request(
            endPoint = "${ApiEndParams.GET_APPOINTMENT.value}${facilityId}",
            method = API.RequestMethod.GET,
            headers = basicHeaders,
            onSuccess = { response ->
                println("DEBUG RESPONSE $response")
                checkIsSuccess(response) { check, message, data ->
                    if (check) {
                        onSuccessCase(data, message)
                    } else {
                        onErrorCase(message, 400)
                    }
                }
            },
            onFailure = { resultResponse, _, statusCode ->
                println("DEBUG ERROR $resultResponse $statusCode")
                onErrorCase(resultResponse, statusCode)
            }
        )
    }

    fun uploadFile(
        file: File,
        onSuccessCase: onSuccessCase,
        onErrorCase: onErrorCase
    ) {
        API.uploadFile(
            endPoint = ApiEndParams.FILE_UPLOAD.value,
            method = API.RequestMethod.POST,
            headers = basicHeadersFile,
            file = file,
            onSuccess = { response ->
                println("DEBUG RESPONSE $response")
                checkIsSuccess(response) { check, message, data ->
                    if (check) {
                        onSuccessCase(data, message)
                    } else {
                        onErrorCase(message, 400)
                    }
                }
            },
            onFailure = { resultResponse, _, statusCode ->
                println("DEBUG ERROR $resultResponse $statusCode")
                onErrorCase(resultResponse, statusCode)
            }
        )
    }

    fun getAgoraToken(
        appointmentId: String,
        onSuccessCase: onSuccessCase,
        onErrorCase: onErrorCase
    ) {

        API.request(
            endPoint = "${ApiEndParams.AGORA_API.value}${appointmentId}/token",
            method = API.RequestMethod.GET,
            headers = basicHeaders,
            onSuccess = { response ->
                println("DEBUG RESPONSE $response")
                checkIsSuccess(response) { check, message, data ->
                    if (check) {
                        onSuccessCase(data, message)
                    } else {
                        onErrorCase(message, 400)
                    }
                }
            },
            onFailure = { resultResponse, _, statusCode ->
                println("DEBUG ERROR $resultResponse $statusCode")
                onErrorCase(resultResponse, statusCode)
            }
        )
    }

    private fun checkIsSuccess(
        response: String, checkCases: ((
            check: Boolean,
            message: String,
            data: String
        ) -> Unit)
    ) {
        val jSon = JSONObject(response)
        val successBool = jSon.optBoolean("success")
        val message = jSon.optString("message") ?: ""
        val data = jSon.opt("data")?.toString() ?: ""
        checkCases(successBool, message, data)
    }
}

enum class ApiEndParams(val value: String) {
    GET_PROVIDER("businesses/storefront"),
    LOGIN("auth/login"),
    SET_APPOINTMENT_DATA("appointments/"),
    GET_APPOINTMENT("appointments/today?facility="),
    FILE_UPLOAD("files/examination"),
    AGORA_API("appointments/")
}