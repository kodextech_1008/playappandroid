package com.idev.number.playapp_android.utils

import android.os.Build

object Constants {
    //    const val TAG = "StarStudentTag"
    const val BASE_URL = "https://api-dev-carepathwaytech.alphatrend.ai"
    const val BaseSocket = "https://api-dev-carepathwaytech.alphatrend.ai"
//        get() {
//            return if (isProduction) {
//                "Socket  :  " // production
//            } else {
//                "https://gabooja.dev:443/" // dev
//            }
//        }//= "https://api.gabooja.com/" // production
    /*    const val USER_TOKEN = "user_token"
        const val INTRO = "intro"
        const val PREFS_TOKEN_FILE = "prefs_token_file"

        val BG_COLORS = arrayOf("#fef9ed", "#f2f8ff", "#edffee", "#fce9ff")
        const val EMAIL="email"*/
}

object App {

    //If you are using your own custom Bluetooth connection code，set this parameter to true；
    //If you are using Bluetooth connection code had been built in SDK library,set this parameter to false;
    //You can see in this demo project separately how to build the code when this boolean parameter value is true or false.
    const val isUseCustomBleDevService = false

    const val isDebugApp = true

    //1 Inch = 2.54 cm = 25.4 mm
    const val mm2Inches = 1 / 25.4f


    fun isAboveAndroidOS_06(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
    }

    fun isAboveAndroidOS_09(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
    }

    fun isAboveAndroidOS_12(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.S
    }
}