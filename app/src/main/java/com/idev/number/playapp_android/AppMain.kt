package com.idev.number.playapp_android

import android.app.Application
import android.content.Context
import com.idev.number.playapp_android.utils.App
import com.idev.number.playapp_android.utils.Constants
import com.libraries.coders.comcodersapiwrapper.API
import com.orhanobut.hawk.Hawk
import com.posthog.PostHog
import com.posthog.android.PostHogAndroid
import com.posthog.android.PostHogAndroidConfig
import io.sentry.Sentry
import io.sentry.SentryLevel


class AppMain : Application() {
    companion object {
        const val POSTHOG_API_KEY = "phc_lWlyKVnkFshWdXh95zWaBXIxB78xlZPNqjY2Guwdoj"

        // usually 'https://us.i.posthog.com' or 'https://eu.i.posthog.com'
        const val POSTHOG_HOST = "https://app.posthog.com"

        lateinit var appContext:Context
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)

    }

    override fun onCreate() {
        super.onCreate()
        appContext = this
        Hawk.init(this).build();

        API.initManager(
            basePath = Constants.BASE_URL,
            baseHeaders = mapOf("Content-Type" to "application/json"),
            debug = App.isDebugApp
        )
        API.LOG_DEBUG_KEY = "VideoConference"
        val config = PostHogAndroidConfig(
            apiKey = POSTHOG_API_KEY,
            host = POSTHOG_HOST
        )
        config.captureApplicationLifecycleEvents = true
        config.captureScreenViews = true
        PostHogAndroid.setup(this, config)
        Sentry.configureScope { scope -> scope.level = SentryLevel.DEBUG }


    }

}


fun phCapture(event: String) {
    PostHog.capture(event)
}

fun phCapture(event: String, properties: Map<String, Any>?) {
    PostHog.capture(event, properties = properties)
}

fun phIdentify(userId: String, phone: String) {

    PostHog.identify(userId, userProperties = mapOf(Pair("phone", phone)))
}

fun phReset() {

    PostHog.reset()
}

fun addMsg(msg: String = "Screen Shot Captured") {

    Sentry.captureMessage(msg)
}