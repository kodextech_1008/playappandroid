package com.idev.number.playapp_android.networks

import androidx.core.view.isVisible
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.idev.number.playapp_android.adapter.MyPlaysAdapter
import com.idev.number.playapp_android.databinding.ActivityMainBinding
import com.idev.number.playapp_android.phCapture
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.util.Date
import java.util.UUID

class PlayNavigator(
    plays: List<Play> = emptyList(),
    error: UserError? = null,
    doneFirstLoad: Boolean = false,
    val binding: ActivityMainBinding
) : ViewModel() {

    private val gateway = BackendGateway(KeychainStore())

    private val _destination =
        MutableStateFlow(PlayDestination("about:blank", UUID.randomUUID().toString(), Date()))
    val destination: StateFlow<PlayDestination> = _destination.asStateFlow()

    private val _needsRefresh = MutableStateFlow(false)
    val needsRefresh: StateFlow<Boolean> = _needsRefresh.asStateFlow()

    private val _plays = MutableStateFlow(plays)
    val plays: StateFlow<List<Play>> = _plays.asStateFlow()

    private val _error = MutableStateFlow(error)
    val error: StateFlow<UserError?> = _error.asStateFlow()

    private val _doneFirstLoad = MutableStateFlow(doneFirstLoad)
    val doneFirstLoad: StateFlow<Boolean> = _doneFirstLoad.asStateFlow()

    fun navigateToUrl(url: String, id: String) {
        _destination.value = PlayDestination(url, id, Date())
    }

    suspend fun fetchSamplePlay(): Play {
        return gateway.fetchSamplePlay()
    }

    private suspend fun fetchLatestPlays(reason: RefreshReason): List<Play> {
        val response = gateway.fetchPlays(reason.name)
        return response.plays
    }

    //    fun markPlayAsRead(id: String) {
//        _plays.value = _plays.value.map { if (it.id == id) it.copy(read = true) else it }
//    }
    val isEmpty: Boolean
        get() {
            return mAdapter?.play?.isEmpty() == true
        }
    var mAdapter: MyPlaysAdapter? = null
    fun setupRecyclerView() {
        binding.playsRecycler.layoutManager =
            LinearLayoutManager(binding.root.context, RecyclerView.VERTICAL, false)
        mAdapter = MyPlaysAdapter(binding.root.context, _plays.value)
        binding.playsRecycler.adapter = mAdapter
        simpleRefresh()
    }

    fun simpleRefresh() {
        var oldPLays =
            gateway.keychain.getPlays(gateway.keychain.authToken)//getPlays(userPhone: keychain.authToken)
        var playItems = groupPlaysByTitle(oldPLays)//oldPLays//
        _plays.value = playItems
        val oldCount = mAdapter?.itemCount
        mAdapter?.notifyItemRangeRemoved(0, oldCount ?: 0)
        mAdapter?.play = playItems
        mAdapter?.notifyItemRangeInserted(0, mAdapter?.itemCount ?: 0)
    }

    fun refreshPlayList(reason: RefreshReason) {
        binding.rlFresh.isRefreshing = true
        viewModelScope.launch {
            _needsRefresh.value = false
            try {
                val latestPlays = fetchLatestPlays(reason)
                var oldPlays =
                    gateway.keychain.getPlays(gateway.keychain.authToken)//getPlays(userPhone: keychain.authToken)
                var oldPlaysMap = oldPlays.associateBy { it.id }.toMutableMap()
                for (playItem in latestPlays) {
                    oldPlaysMap[playItem.id] = playItem
                }
                // Convert map values back to list
                oldPlays = oldPlaysMap.values.toList()
                gateway.keychain.savePlays(oldPlays, gateway.keychain.authToken)
                binding.rlFresh.isRefreshing = false
                simpleRefresh()
//                _plays.value = latestPlays
                _error.value = null
                if (mAdapter?.play?.isEmpty() == true) {
                    binding.rlEmpty.isVisible = true
                    phCapture("android_refresh_empty", mapOf("reason" to reason.name))
                } else {
                    binding.rlEmpty.isVisible = false
                }

            } catch (e: Exception) {
                binding.rlEmpty.isVisible = false
//                _plays.value = emptyList()

                binding.rlFresh.isRefreshing = false
                simpleRefresh()
                if (_plays.value.isEmpty()) {
                    _error.value = UserError("Failed to fetch latest plays.", Date(), e)
                } else {
                    _error.value = null
                }
                phCapture(
                    "android_refresh_error",
                    mapOf("reason" to reason.name, "error" to e.toString())
                )
            }
        }
    }

    fun resetPlayList() {
//        _plays.value = emptyList()
        simpleRefresh()
        _error.value = null
        _doneFirstLoad.value = false
    }
}

data class PlayDestination(val url: String, val id: String, val `when`: Date)

enum class RefreshReason {
    APP_FOREGROUND, DRAG_REFRESH, FIRST_LOAD, PUSH_RECEIVED
}

class UserError {
    //: LocalizedError
    var errorDescription: String? = ""
    var timestamp: Date = Date()
    var cause: Exception? = Exception()

    constructor(errorDescription: String?, timestamp: Date, cause: Exception?) {
        this.errorDescription = errorDescription
        this.timestamp = timestamp
        this.cause = cause
    }
//    init(_ localizedDescription: String) {
//        self.errorDescription = localizedDescription
//        self.timestamp = Date()
//    }
//
//    init(_ localizedDescription: String, cause: Error) {
//        self.init(localizedDescription)
//        self.cause = cause
//    }

//    var timestampString: String {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        return dateFormatter.string(from: timestamp)
//    }
}