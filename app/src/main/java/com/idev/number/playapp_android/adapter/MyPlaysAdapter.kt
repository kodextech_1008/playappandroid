package com.idev.number.playapp_android.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.idev.number.playapp_android.R
import com.idev.number.playapp_android.databinding.ItemMyPlaysBinding
import com.idev.number.playapp_android.networks.BackendGateway
import com.idev.number.playapp_android.networks.KeychainStore
import com.idev.number.playapp_android.networks.Play
import com.idev.number.playapp_android.ui.PlayDetailActivity
import com.idev.number.playapp_android.ui.SubListingActivity
import com.idev.number.playapp_android.utils.humanizedDateStr

class MyPlaysAdapter(
    private val context: Context,
    var play: List<Play>,
    private var isSubPlay: Boolean = true
) :
    RecyclerView.Adapter<MyPlaysVH>() {
    private val gateway = BackendGateway(KeychainStore())
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyPlaysVH {
        return MyPlaysVH(
            LayoutInflater.from(context).inflate(R.layout.item_my_plays, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return play.count()
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyPlaysVH, position: Int) {
        val item = play[holder.absoluteAdapterPosition]
        val readCheck = if (!isSubPlay) {
            item.isAllRead == 0
        } else (item.read == true)
        val read = readCheck//item.read ?: false

        holder.binding.tvTitle.text = item.title
        holder.binding.tvTitle.setTextColor(
            if (!read) context.getColor(R.color.main_color) else context.getColor(
                R.color.black
            )
        )
        holder.binding.timeTxt.text = humanizedDateStr(item.published)
        holder.binding.tvSubTitle.text = item.body
        holder.binding.tvSubTitle.setTextColor(
            if (!read) context.getColor(android.R.color.black) else context.getColor(
                android.R.color.darker_gray
            )
        )
        holder.binding.timeTxt.setTextColor(
            if (!read) context.getColor(android.R.color.black) else context.getColor(
                android.R.color.darker_gray
            )
        )
        if (item.isSub == true || isSubPlay) {
            holder.binding.onlineRl.isVisible = !read
            holder.binding.tvBadge.isVisible = false
        } else {
            holder.binding.tvBadge.isVisible = true
//            holder.binding.tvBadge.text = "${if(item.isAllRead == 0) item.subPlays?.count() else item.isAllRead}"
            holder.binding.tvBadge.setBadgeCount(
                "${if (item.isAllRead == 0) item.subPlays?.count() else item.isAllRead}",
                true
            )
//            holder.binding.tvBadge.badgeBackgroundColor =
//                if (item.isAllRead != 0) context.getColor(R.color.main_color) else context.getColor(
//                    android.R.color.darker_gray
//                )
            holder.binding.tvBadge.paintBackground.color =
                if (item.isAllRead != 0) context.getColor(R.color.main_color) else context.getColor(
                    android.R.color.darker_gray
                )
//            holder.binding.tvBadge.setBackgroundColor(if (item.isAllRead != 0) context.getColor(R.color.main_color) else context.getColor(
//                android.R.color.darker_gray
//            ))
            holder.binding.tvBadge.invalidate()

            holder.binding.onlineRl.isVisible = item.isAllRead != 0
        }

        try {
            Glide.with(holder.binding.ivUser)
                .load(item.thumbnail)
                .placeholder(R.drawable.profile_pic)
                .error(R.drawable.profile_pic)
                .circleCrop()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(holder.binding.ivUser)
        } catch (ignore: Exception) {
        }
        holder.itemView.setOnClickListener {
            if (isSubPlay) {
                val oldPLays =
                    gateway.keychain.getPlays(gateway.keychain.authToken)//getPlays(userPhone: keychain.authToken)
                val index = oldPLays.indexOfFirst { it.id == item.id }
                if (index != -1) {
                    oldPLays[index].read = true
                }
                gateway.keychain.savePlays(oldPLays, gateway.keychain.authToken)
                play[holder.absoluteAdapterPosition].read = true
                notifyItemChanged(holder.absoluteAdapterPosition)
                val intent = Intent(context, PlayDetailActivity::class.java)
                intent.putExtra("URL", item.url)
                intent.putExtra("ID", item.id)
                context.startActivity(intent)
            } else {
                val intent = Intent(context, SubListingActivity::class.java)
//                intent.putExtra("PLAY", item)
                SubListingActivity.subListItem = item
                context.startActivity(intent)
            }

        }

    }
}

class MyPlaysVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val binding = ItemMyPlaysBinding.bind(itemView)

}