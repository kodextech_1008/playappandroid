package com.idev.number.playapp_android.networks

import android.app.NotificationManager
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.PowerManager
import android.provider.Settings
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.preference.Preference
import com.google.firebase.messaging.FirebaseMessaging
import com.idev.number.playapp_android.MyFirebaseInstanceIDService
import com.idev.number.playapp_android.phCapture
import com.idev.number.playapp_android.phIdentify
import com.idev.number.playapp_android.phReset

class UserRepository(
    val context: Context,
    val requestPermissionLauncher: ActivityResultLauncher<String>
) {

    private val gateway: BackendGateway = BackendGateway(KeychainStore())
    private val sharedPreferences: SharedPreferences? =
        Preference(context).sharedPreferences

    // Login state

    var verifiedNumber: String?
        get() = gateway.keychain.verifiedphoneNumber
        private set(value) {
            gateway.keychain.verifiedphoneNumber = value
        }

    val isLoggedIn: Boolean
        get() {
            val oneCond = verifiedNumber != null
            val towCond = gateway.keychain.authToken != null
            return oneCond && towCond
        }

    private fun login(phoneNumber: String, authToken: String) {
        verifiedNumber = phoneNumber
        gateway.keychain.authToken = authToken
        // Notify observers about login state change
        // This depends on how you implement ObservableObject in Kotlin
    }

    private fun identify(userId: String?) {
        userId?.let { uid ->
            verifiedNumber?.let { phoneNumber ->
                phIdentify(uid, phone = phoneNumber)
            }
        }
    }

    fun logout() {
        verifiedNumber = null
        gateway.keychain.authToken = null
        phReset()
        // Notify observers about login state change
        // This depends on how you implement ObservableObject in Kotlin
    }

    // Push state

    var pushEnabled: Boolean
        get() = sharedPreferences?.getBoolean("pushEnabled", false) ?: false
        private set(value) {
            sharedPreferences?.edit()?.putBoolean("pushEnabled", value)?.apply()
        }

    private var pushRegistered: Boolean
        get() = sharedPreferences?.getBoolean("pushRegistered", false) ?: false
        set(value) {
            sharedPreferences?.edit()?.putBoolean("pushRegistered", value)?.apply()
        }

    private suspend fun requestPushPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(
                    context,
                    android.Manifest.permission.POST_NOTIFICATIONS
                ) ==
                PackageManager.PERMISSION_GRANTED
            ) {
//                Log.e(TAG, "PERMISSION_GRANTED")
                // FCM SDK (and your app) can post notifications.
                return true
            } else {
//                Log.e(TAG, "NO_PERMISSION")
                // Directly ask for the permission
                requestPermissionLauncher.launch(android.Manifest.permission.POST_NOTIFICATIONS)
                return false

            }
        } else {
            return true
        }

    }

    suspend fun hasPushPermission(): Boolean = isPushNotificationEnabled(context = context)

    private fun registerWithAPNs() {
        // Implementation of registerWithAPNs function
        // It should register the device for push notifications
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            setupToken(it)
            Log.d(MyFirebaseInstanceIDService.TAG, "${MyFirebaseInstanceIDService.TAG}, token: $it")
        }
    }

    suspend fun refreshPush() {
        if (pushEnabled) {
            val hasPermission = hasPushPermission()
            if (hasPermission) {
                registerWithAPNs()
            }
        }
    }

    suspend fun enablePush(): Boolean {

        val granted = requestPushPermission()
        if (granted) {
            pushEnabled = true
            registerWithAPNs()
        }
        phCapture("android_push_enabled", properties = mapOf("permission_granted" to granted))
        // Notify observers about push state change
        // This depends on how you implement ObservableObject in Kotlin
        return granted
    }

    suspend fun disablePush() {
        pushEnabled = false
        deregisterDevice()
        phCapture("android_push_disabled")
        // Notify observers about push state change
        // This depends on how you implement ObservableObject in Kotlin
    }

    // API calls

    suspend fun sendAuthCode(phoneNumber: String): Boolean {
        try {
            phCapture("android_2fa_verify_sent", properties = mapOf("phone" to phoneNumber))
            return gateway.sendCode(phoneNumber)

        } catch (e: Exception) {
            // Handle exception
            e.printStackTrace()
            return false
        }
    }

    suspend fun verify(phoneNumber: String, code: String): Boolean {
        try {
            val response = gateway.verifyCode(phoneNumber, code)
            if (response.valid && response.token != null) {
                login(phoneNumber, response.token)
                identify(response.userid)
                return true
            }
            phCapture("android_2fa_verify_failed", properties = mapOf("phone" to phoneNumber))
        } catch (e: Exception) {
            // Handle exception
            e.printStackTrace()
            return false
        }
        return false
    }

    suspend fun handshake(callBack: ((Boolean) -> Unit)) {
        if (!isLoggedIn) {
            callBack.invoke(false)
            return
        }
        try {
            val response = gateway.handshake()
            if (!response.valid) {
                callBack.invoke(false)
                logout()
            }
            if (response.valid) {
                callBack.invoke(true)
                response.token?.let { token ->
                    if (token.isNotEmpty()) {
                        gateway.keychain.authToken = token
                    }
                }
                identify(response.userid)
                if (response.msg == "apns-refresh") {
                    refreshPush()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            callBack.invoke(false)
            // Handle exception
        }
    }

    fun setupToken(token: String) {
        gateway.keychain.deviceToken = token
    }

    suspend fun registerDevice() {//token: String = gateway.keychain.deviceToken
//        gateway.keychain.deviceToken = gateway.keychain.deviceToken
        if (gateway.keychain.deviceToken?.isNotEmpty() == true) {
            try {
                gateway.registerDevice(gateway.keychain.deviceToken ?: "")
                pushRegistered = true
            } catch (e: Exception) {
                // Handle exception
            }
        }
    }


    private suspend fun deregisterDevice() {
        gateway.keychain.deviceToken?.let { token ->
            try {
                gateway.deregisterDevice(token)
                pushRegistered = false
            } catch (e: Exception) {
                // Handle exception
            }
        }
    }
}


fun isPushNotificationEnabled(context: Context): Boolean {
    val appContext = context.applicationContext
    val packageName = context.packageName
    val notificationManagerCompat = androidx.core.app.NotificationManagerCompat.from(appContext)

    // For devices running Android Oreo (API 26) or higher, you need to check if the app has the
    // notification channel enabled
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val notificationChannelGroup = notificationManagerCompat.getNotificationChannelGroup(
            NotificationManagerCompat.EXTRA_USE_SIDE_CHANNEL
//            NotificationManagerCompat.GROUP_SYSTEM
        )
        if (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                notificationChannelGroup?.isBlocked == true
            } else {
                return false
            }
        ) {
            // Notification channel is blocked, push notifications are disabled
            return false
        }
    }

    // For devices running Android Marshmallow (API 23) or higher, you need to check if the app
    // has been granted the "notification policy access" permission
    //        if (!notificationManagerCompat.isNotificationPolicyAccessGranted) {
    if (!notificationManagerCompat.areNotificationsEnabled()) {
        // Notification policy access is not granted, push notifications are disabled
        return false
    }

    // For devices running Android KitKat (API 19) or higher, you need to check if the app has
    // the "show notifications" permission granted


    // For devices running Android Lollipop (API 21) or higher, you need to check if the app
    // has the "show notifications" permission granted in the app settings
    val appNotificationSettings =
        appContext.getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
    if (appNotificationSettings != null) {
        if (!appNotificationSettings.areNotificationsEnabled()) {
            // App notifications are not enabled, push notifications are disabled
            return false
        }
    }

    // For devices running Android Marshmallow (API 23) or higher, you need to check if the app
    // has the "battery optimization" disabled in the app settings
    val powerManager = appContext.getSystemService(Context.POWER_SERVICE) as? PowerManager
    powerManager?.let {
        if (!it.isIgnoringBatteryOptimizations(packageName)) {
            // Battery optimization is enabled, push notifications might be disabled
            return false
        }
    }


    // For devices running Android Lollipop (API 21) or higher, you need to check if the app has
    // the "do not disturb" mode enabled
    val notificationManager =
        appContext.getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
    if (notificationManager != null && notificationManager.currentInterruptionFilter != NotificationManager.INTERRUPTION_FILTER_ALL) {
        // Do not disturb mode is enabled, push notifications are disabled
        return false
    }


    // For devices running Android KitKat (API 19) or higher, you need to check if the app
    // has the "notification access" permission granted
    val notificationListenerServices =
        Settings.Secure.getString(appContext.contentResolver, "enabled_notification_listeners")
    if (notificationListenerServices.isNullOrEmpty() || !notificationListenerServices.contains(
            appContext.packageName
        )
    ) {
        // Notification access is not granted, push notifications might be disabled
        return false
    }

    return true
}