package com.idev.number.playapp_android.utils

import android.content.ContentResolver
import android.content.Context
import android.database.ContentObserver
import android.net.Uri
import android.os.Handler
import android.provider.MediaStore

class ScreenshotObserver(
    handler: Handler,
    private val context: Context,
    private val listener: (Uri?) -> Unit
) : ContentObserver(handler) {

    private val contentResolver: ContentResolver = context.contentResolver

    override fun onChange(selfChange: Boolean) {
        super.onChange(selfChange)
        checkScreenshots()
    }

    private fun checkScreenshots() {
        val projection = arrayOf(MediaStore.Images.ImageColumns._ID)
        val selection = "${MediaStore.Images.ImageColumns.DATA} LIKE ?"
        val selectionArgs = arrayOf("%/Screenshots/%")
        val sortOrder = "${MediaStore.Images.ImageColumns.DATE_ADDED} DESC"

        contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            projection,
            selection,
            selectionArgs,
            sortOrder
        )?.use { cursor ->
            if (cursor.moveToFirst()) {
                val id = cursor.getLong(cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID))
                val uri = Uri.withAppendedPath(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    id.toString()
                )
                listener.invoke(uri)
            } else {
                listener.invoke(null)
            }
        }
    }

    fun start() {
        contentResolver.registerContentObserver(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            true,
            this
        )
    }

    fun stop() {
        contentResolver.unregisterContentObserver(this)
    }
}
